This folder contains the code and data for reproducing results in the paper, Sung et al. (2019+) "Calibration for Computer Experiments with Binary Responses and Application to Cell Adhesion Study".

---

## Install Packages
Before running the code, users need to install the R package "calibrateBinary" which was developed for implementing the calibration method in the paper. The package can be installed by unzipping the "calibrateBinary.zip" to the working directory and then typing the command line "install.packages("calibrateBinary_0.2.tar.gz")" in R. Other necessary R packages for the code include 

1.	kernlab (>=0.9-25)
2.	lhs (>=0.15)
3.	sensitivity (>=1.15.2)
4.	numDeriv (>=2016.8-1)
5.	lattice (>=0.20-35)
6.	calibrateBinary (>=0.2)
7.	ggplot2 (>=3.0.0)
8.	randomForest (>=4.6-14),

which can be directly downloaded and installed from R CRAN.

---

## Description of Each File
This folder contains three R code files: "simulation_2d.R", "simulation_5d.R", and "realdata.R", and two data files: "labdata.txt" and "simdata.txt". The description of each file is given below.

1. File "simulation_2d.R" can reproduce Tables 1 and Figures 3-4, which is running simulated examples of 2-dimensional computer experiments and comparing the proposed method with a naive approach. Running the code which conducts 100 replicates of the examples would take approximately 600 minutes. Users may choose fewer replicates of the examples in line 4 or run the 100 replicates simultaneously if parallel computing is available, to speed up the whole process.
2. File "simulation_5d.R" can reproduce Tables 2, which is running simulated examples of 5-dimensional computer experiments. Running the code which conducts 100 replicates of the examples would take approximately 1200 minutes. Users may choose fewer replicates of the examples in line 4 or run the 100 replicates simultaneously if parallel computing is available, to speed up the whole process.
3. File "realdata.R" analyzes the T cell adhesion experiments based on the proposed calibration method in the paper. This file can reproduce Figures 5-7, which illustrates the raw data, fitted surface for the physical experiments, sensitivity analysis for the computer experiments, and the comparison of the fitted models from the physical and computer experiments. This file also returns the estimated calibration parameters and their corresponding standard deviations for the T cell adhesion experiments. Running the code would take approximately 30 minutes.
4. File "labdata.txt" contains the (lab) experimental data for the T cell adhesion experiments. Two control variables, "tc" and "tw", are in the first and second columns of the data, and the corresponding binary output of each combination of the two control variables, "y", is in the third column. There are 272 samples (i.e., 272 rows).
5. File "simdata.txt" contains the simulation data for the T cell adhesion experiments. Six control variables, "tc", "tw", "kc", "kf", "kr", and "krx", are in columns 1-6 of the data, and the corresponding binary output of each combination of the six control variables, "ys", is in the 7th column. There are 1200 samples (i.e., 1200 rows).
