library(calibrateBinary)
library(lattice)
library(numDeriv)
library(sensitivity)
library(kernlab)
library(ggplot2)

##### read experimental and simulated data #####
labdata.df <- read.table("labdata.txt", header = TRUE, stringsAsFactors = FALSE)
simdata.df <- read.table("simdata.txt", header = TRUE, stringsAsFactors = FALSE)

##### standardizing input variables #####
# lab data #
xp <- labdata.df[,1:2]
range.diff.xp <- apply(xp, 2, function(x) diff(range(x)))
min.xp <- apply(xp, 2, min)
xp <- apply(xp, 2, FUN = function(x) (x-min(x))/diff(range(x)))
yp <- labdata.df[,3]

# simulation data #
xs <- simdata.df[,1:6]
xs[,3:6] <- apply(xs[,3:6], 2, log)      # xs is LHD after using log transformation
min.xs <- apply(xs, 2, min)
min.xs[1:2] <- min.xp[1:2]               # consistent with xp's mins
range.diff.xs <- apply(xs, 2, function(x) diff(range(x)))
range.diff.xs[1:2] <- range.diff.xp[1:2] # consistent with xp's ranges
for(i in 1:6) xs[,i] <- (xs[,i] - min.xs[i])/range.diff.xs[i]
ys <- simdata.df[,7]

##### run calibration: equation (8) #####
set.seed(1)
calibrate.result <- calibrateBinary(xp, yp, xs[,1:2], xs[,3:6], ys, 
                                    lower = rep(0,6), upper = rep(1,6))
theta.hat <- calibrate.result$calibration[1,1:4]

theta.hat <- exp(theta.hat * range.diff.xs[3:6] + min.xs[3:6]) # scale back to original units
print(format(theta.hat,digits = 3))

##### plot the data in the physical experiments: Figure 5 (left) #####
par(mfrow = c(1,1))
par(xpd = T, mar = par()$mar + c(0,0,0,0))
plot(labdata.df[,1:2], 
     col = ifelse(labdata.df[,3], "red", "blue"), 
     pch = labdata.df[,3]*2 + 1,
     xlab = expression(italic(x[T[c]])), ylab = expression(italic(x[T[w]])))
legend("topright",
       c("Adhesion", "No adhesion"), pch = c(3,1), cex = 0.8,
       col = c("red", "blue"), inset=c(0,-0.1), horiz=TRUE, bty="n",text.width = 1)

##### plot eta hat: Figure 5 (right) #####
cat("Estimated parameters for eta hat ... \n")
print(calibrate.result$para.physics)
lambda <- calibrate.result$para.physics$lambda
rho <- calibrate.result$para.physics$rho
nu <- calibrate.result$para.physics$nu

x.test <- expand.grid(seq(0,1,0.01), seq(0,1,0.01))
etahat <- KLR(xp, yp, x.test, lambda = lambda, rho = rho, nu = nu)
x1.test <- x.test[,1] * range.diff.xp[1] + min.xp[1] # scale to original units
x2.test <- x.test[,2] * range.diff.xp[2] + min.xp[2] # scale to original units
wireframe(etahat ~ x1.test * x2.test, 
          xlab = expression(italic(x[T[c]])), 
          ylab = expression(italic(x[T[w]])), 
          zlab = expression(hat(italic(eta))[italic(n)](italic(x))), 
          scales = list(arrows=FALSE, tick.number = 4), zlim = c(0.25,0.65))


##### plot p hat with estimated calibration parameters: Figure 7 #####
xs.test <- cbind(x.test, matrix(calibrate.result$calibration[1,1:4], ncol = 4, nrow = nrow(x.test), byrow = TRUE))  # plug in calibration parameters

cat("Estimated parameters for p hat ... \n")
print(calibrate.result$para.simulation)
sigma.hat <- calibrate.result$para.simulation$sigma 
gp.fit <- gausspr(xs, as.factor(ys), kpar = list(sigma = sigma.hat),
                  type = "classification", scaled = FALSE, cross = 0, fit = FALSE)
pshat <- predict(gp.fit, xs.test, type = "probabilities")[,2]
cat("L2 distance =", sqrt(mean((pshat - etahat)^2)), "\n")

par(mfrow = c(1,2))
par(mar = c(5,2,1.5,1.5))

plot(aggregate(y ~ x, data = data.frame("x" = x1.test, "y" = pshat), mean), 
     ylim = c(0.3,0.6), type = "l", xlab = "x \n (a) contact time", ylab = "",
     col = 2, lty = 2, lwd = 2, yaxt = "n")
axis(2, at = (3:6)/10)
lines(aggregate(y ~ x, data = data.frame("x" = x1.test, "y" = etahat), mean), 
       type = "l", lwd = 2)
legend("topright", 
       legend=c(expression(hat(italic(eta))[italic(n)](italic(x))), expression(hat(italic(p))[italic(n)](italic(x),hat(theta)))), lty=c(1,2), 
       col=c(1,2), lwd = c(2,2), horiz=TRUE, bty='n', cex=1)

plot(aggregate(y ~ x, data = data.frame("x" = x2.test, "y" = pshat), mean),
     ylim = c(0.3,0.6), type = "l", xlab = "x \n (b) waiting time", ylab = "",
     col = 2, lty = 2, lwd = 2, yaxt = "n")
axis(2, at = (3:6)/10)
lines(aggregate(y ~ x, data = data.frame("x" = x2.test, "y" = etahat), mean), 
       type = "l", lwd = 2)
legend("topright", legend=c(expression(hat(italic(eta))[italic(n)](italic(x))), expression(hat(italic(p))[italic(n)](italic(x),hat(theta)))), lty=c(1,2), 
       col=c(1,2), lwd = c(2,2), horiz=TRUE, bty='n', cex=1)


##### compute the standard deviation of the estimated calibration parameters #####
q <- length(theta.hat)
theta.hat.ori <- calibrate.result$calibration[1,1:4] # theta hat before scaling back

V <- matrix(0, q, q)
for(i in 1:nrow(x.test)){
  phat_theta <- function(theta){
    x.new <- cbind(x.test[i,,drop=FALSE], matrix(theta, ncol = q, nrow = 1, byrow = TRUE))
    phat <- predict(gp.fit, x.new, type = "probabilities")[,2]
    drop(phat)
  }
  V <- V + hessian(function(theta) {(etahat[i] - phat_theta(theta))^2}, theta.hat.ori)
}
V <- V/nrow(x.test)

W <- matrix(0, q, q)
for(i in 1:nrow(x.test)){
  phat_theta <- function(theta){
    x.new <- cbind(x.test[i,,drop=FALSE], matrix(theta, ncol = q, nrow = 1, byrow = TRUE))
    phat <- predict(gp.fit, x.new, type = "probabilities")[,2]
    drop(phat)
  }
  dp <- grad(function(theta) {phat_theta(theta)}, theta.hat.ori)
  W <- W + etahat[i] * (1 - etahat[i]) * matrix(dp,ncol=1) %*% matrix(dp,nrow=1)
}
W <- W/nrow(x.test)

Var.asymptotic <- diag((solve(V) %*% W %*% solve(V) * 4)/length(yp))
mu.normal <- range.diff.xs[3:6] * theta.hat.ori + min.xs[3:6]
sig2.normal <- range.diff.xs[3:6]^2 * Var.asymptotic
sd.lognormal <- sqrt((exp(sig2.normal) - 1) * exp(2*mu.normal + sig2.normal)) # log xs is normal so use the sd of log-normal
cat("standard deviation of estimated calibration parameters: ", sd.lognormal, "\n")


##### plot sensitivity analysis for computer experiment data: Figure 6 #####
set.seed(1)
phat.fun <- function(X){
  predict(gp.fit, X, type = "probabilities")[,2]
}
n <- 5000
X1 <- data.frame(matrix(runif(6 * n), nrow = n))
X2 <- data.frame(matrix(runif(6 * n), nrow = n))
# sensitivity analysis
x <- sobol(model = phat.fun, X1 = X1, X2 = X2, order = 1, nboot = 100)
data.estimates = data.frame(
  var   = colnames(simdata.df)[3:6],
  par = x$S[3:6,1],
  se = x$S[3:6,3])
data.estimates$idr <- data.estimates$par
data.estimates$upper <- data.estimates$par + (1.96*data.estimates$se)
data.estimates$lower <- data.estimates$par - (1.96*data.estimates$se)

p2 <- ggplot(data.estimates, aes(var,idr, size=10), show.legend = FALSE) + theme_bw(base_size=20)
p2 + geom_point(show.legend = FALSE) +
  geom_errorbar(aes(x = var, ymin = lower, ymax = upper, size=2), width = 0.2, show.legend = FALSE) + 
  xlab("") + ylab("Sobol' sensitivity indices") + 
  scale_x_discrete(labels=c("kc" = expression(italic(x[K[c]])), 
                            "kf" = expression(italic(x[K[f]])), 
                            "kr" = expression(italic(x[K[r]])), 
                            "krx" = expression(italic(x[K[paste(r,",",p)]]))))

